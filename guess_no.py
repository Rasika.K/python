import random
top_no=input("type a number: ")
if top_no.isdigit():
    top_no=int(top_no)
    if top_no <= 0:
        print("enter no greater than zero")
        quit()
else:
    print("please type number")
    quit()

random_number=random.randint(0,top_no)
guesses=0
while True:
    guesses+=1
    guess_no=input("guess the number: ")
    if guess_no.isdigit():
        guess_no=int(guess_no)
    else:
        print("type number")
        continue
    if guess_no==random_number:
        print ("correct")
        break
    else:
        if guess_no>random_number:
            print ("guess is larger than number")
        else :

            print ("guess is smaller than number")
print("you used "+ str(guesses)+(" chances!!"))

